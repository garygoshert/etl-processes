USE `wynn_rdm`;
CREATE TABLE `wynn_rdm`.`wynn_job_list_staging` (
  		`bb_job_location_id` int(11) NOT NULL AUTO_INCREMENT,
  		`job_hk` varchar(50) NOT NULL DEFAULT '',
  		`job_title` varchar(200) NOT NULL,
  		`city` varchar(100) NOT NULL,
  		`state` varchar(100) NOT NULL,
  		`created_on_date` datetime DEFAULT NULL,
  		`insert_date` datetime DEFAULT NULL,
  		PRIMARY KEY (`bb_job_location_id`,`job_title`,`city`,`state`,`job_hk`)
) ENGINE=InnoDB AUTO_INCREMENT=3843 DEFAULT CHARSET=utf8;

CREATE TABLE `wynn_rdm`.`wynn_onet_staging` (
  		`ID` int(11) NOT NULL AUTO_INCREMENT,
  		`job_hk` varchar(50) DEFAULT NULL,
  		`onet_code` varchar(50) DEFAULT NULL,
  		`entity` varchar(20) DEFAULT NULL,
  		`client_id` varchar(20) DEFAULT NULL,
  		`cb_title` varchar(200) DEFAULT NULL,
  		`job_title` varchar(200) DEFAULT NULL,
  		`location` varchar(200) DEFAULT NULL,
  		`confidence` varchar(20) DEFAULT NULL,
  		`hardToHire_index` varchar(20) DEFAULT NULL,
  		`updated_time` datetime DEFAULT NULL,
  		`insert_date` datetime DEFAULT NULL,
  		PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3848 DEFAULT CHARSET=latin1;

USE `wynn_bdas`;
CREATE TABLE `wynn_bdas`.`b_onet` (
  	`onet_id` int(11) NOT NULL AUTO_INCREMENT,
  	`onet_hk` varchar(50) NOT NULL DEFAULT '',
  	`job_hk` varchar(50) NOT NULL DEFAULT '',
  	`insert_date` datetime DEFAULT NULL,
  	PRIMARY KEY (`onet_id`,`job_hk`,`onet_hk`)
) ENGINE=InnoDB AUTO_INCREMENT=3843 DEFAULT CHARSET=latin1;

CREATE TABLE `wynn_bdas`.`d_onet` (
  	`onet_pk` int(11) NOT NULL AUTO_INCREMENT,
  	`job_hk` varchar(50) NOT NULL,
  	`onet_hk` varchar(50) NOT NULL,
  	`cb_title` varchar(200) DEFAULT NULL,
  	`job_title` varchar(200) DEFAULT NULL,
  	`hardtohire_pct` varchar(20) DEFAULT NULL,
  	`insert_date` datetime DEFAULT NULL,
  	PRIMARY KEY (`onet_pk`,`job_hk`,`onet_hk`)
) ENGINE=InnoDB AUTO_INCREMENT=3843 DEFAULT CHARSET=latin1;

